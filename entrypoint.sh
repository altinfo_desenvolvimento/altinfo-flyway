#!/usr/bin/env bash

isCommand() {
  for cmd in \
    "migrate" \
    "clean" \
    "info" \
    "validate" \
    "undo" \
    "baseline" \
    "repair"
  do
    if [ -z "${cmd#"$1"}" ]; then
      return 0
    fi
  done

  return 1
}

# check if the first argument passed in looks like a flag
if [ "$(printf %c "$1")" = '-' ]; then
  set -- /sbin/tini -- flyway "$@"
# check if the first argument passed in is flyway
elif [ "$1" = 'flyway' ]; then
  set -- /sbin/tini -- "$@"
# check if the first argument passed in matches a known command
elif isCommand "$1"; then
  set -- /sbin/tini -- flyway "$@"
fi

exec "$@"